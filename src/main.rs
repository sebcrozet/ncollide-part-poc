extern crate nalgebra as na;
extern crate ncollide3d;

use kiss3d::light::Light;
use kiss3d::window::Window;
use na::{Isometry3, Matrix3, Point3, Rotation3, Translation3, UnitQuaternion, Vector3};
use ncollide3d::narrow_phase::ContactEvent;
use ncollide3d::pipeline::{CollisionGroups, CollisionWorld, GeometricQueryType};
use ncollide3d::shape::{Cuboid, ShapeHandle};
use std::vec::Vec;
use kiss3d::camera::ArcBall;

pub enum Inches {}

#[derive(Debug)]
pub struct Perspective {
    pub position: Point3<f32>,
    pub rotation: Matrix3<f32>
}

impl Perspective {
    fn new(
        x: f32,
        y: f32,
        z: f32,
        rotation: Matrix3<f32>
    ) -> Self {
        Perspective {
            position: Point3::new(x, y, z),
            rotation
        }
    }

    fn to_isometry(&self) -> Isometry3<f32> {
        let rotation = Rotation3::from_matrix_unchecked(self.rotation.transpose());
        let translation = Translation3::new(self.position.x, self.position.y, self.position.z);
        let u = UnitQuaternion::from_rotation_matrix(&rotation);

        Isometry3::from_parts(
            translation,
            u
        )
    }
}

#[derive(Debug)]
pub struct PartDimensions {
    pub width: f32,
    pub height: f32,
    pub length: f32
}

#[derive(Debug)]
pub struct Part {
    pub dimensions: PartDimensions,
    pub part_label: String,
    pub ordinal_position: i32,
    pub perspective: Perspective
}

fn get_example_parts() -> Vec<Part> {
    vec![
        Part {
            dimensions: PartDimensions {
                width: 1.5,
                height: 3.5,
                length: 79.5
            },
            perspective: Perspective::new(-5.5, 41.25, -1.75, Matrix3::new(
                1.0, 0.0, 0.0,
                0.0, 0.0, 1.0,
                0.0, -1.0, 0.0
            )),
            part_label: "A".to_string(),
            ordinal_position: 1
        },
        Part {
            dimensions: PartDimensions {
                width: 1.5,
                height: 3.5,
                length: 79.5
            },
            perspective: Perspective::new(11.25, 41.25, -1.75, Matrix3::new(
                -1.0, 0.0, 0.0,
                0.0, 0.0, 1.0,
                0.0, 1.0, 0.0
            )),
            part_label: "A".to_string(),
            ordinal_position: 2
        },
        Part {
            dimensions: PartDimensions {
                width: 1.5,
                height: 3.5,
                length: 79.5
            },
            perspective: Perspective::new(-11.25, 41.25, -1.75, Matrix3::new(
                1.0, 0.0, 0.0,
                0.0, 0.0, 1.0,
                0.0, -1.0, 0.0
            )),
            part_label: "A".to_string(),
            ordinal_position: 3
        },
        Part {
            dimensions: PartDimensions {
                width: 1.5,
                height: 3.5,
                length: 24.0
            },
            perspective: Perspective::new(0.0, 0.75, -1.75, Matrix3::new(
                0.0, 1.0, 0.0,
                0.0, 0.0, 1.0,
                1.0, 0.0, 0.0
            )),
            part_label: "B".to_string(),
            ordinal_position: 4
        },
        Part {
            dimensions: PartDimensions {
                width: 1.5,
                height: 3.5,
                length: 24.0
            },
            perspective: Perspective::new(0.0, 81.75, -1.75, Matrix3::new(
                0.0, -1.0, 0.0,
                0.0, 0.0, 1.0,
                -1.0, 0.0, 0.0
            )),
            part_label: "C".to_string(),
            ordinal_position: 5
        },
        Part {
            dimensions: PartDimensions {
                width: 1.5,
                height: 3.5,
                length: 24.0
            },
            perspective: Perspective::new(0.0, 83.25, -1.75, Matrix3::new(
                0.0, -1.0, 0.0,
                0.0, 0.0, 1.0,
                -1.0, 0.0, 0.0
            )),
            part_label: "D".to_string(),
            ordinal_position: 6
        }
    ]
}

fn main() {
    // For visualization.
    let mut window = Window::new("ncollide-part-poc window title");
    window.set_background_color(1.0, 1.0, 1.0);
    window.set_light(Light::StickToCamera);

    let parts = get_example_parts();
    let collision_group = CollisionGroups::new()
        .with_membership(&[1])
        .with_whitelist(&[1]);

    let mut world = CollisionWorld::new(0.1);
    let contacts_query = GeometricQueryType::Contacts(0.1, 0.0);

    for part in parts {
        let midpoint = Vector3::new(
            part.dimensions.width / 2.0,
            part.dimensions.height / 2.0,
            part.dimensions.length / 2.0
        );

        let m = part.perspective.to_isometry();
        let g = Cuboid::new(midpoint);
        let sh = ShapeHandle::new(g);

        world.add(
            m,
            sh,
            collision_group,
            contacts_query,
            format!("{} (ord. {})", part.part_label, part.ordinal_position),
        );

        // Add the cuboid to our visualization.
        let mut cube = window.add_cube(midpoint.x * 2.0, midpoint.y * 2.0, midpoint.z * 2.0);
        cube.set_local_transformation(m);

        match part.part_label.as_str() {
            "A" => cube.set_color(1.0, 0.0, 0.0), // Red
            "B" => cube.set_color(0.0, 1.0, 0.0), // Green
            "C" => cube.set_color(1.0, 0.0, 1.0), // Blue
            "D" => cube.set_color(0.0, 1.0, 1.0), // Purple
            _ => cube.set_color(1.0, 0.5, 0.0),   // Orange
        }
    }

    world.update();

    for evt in world.contact_events(){
        if let &ContactEvent::Started(item1, item2) = evt {
            let o1 = world.collision_object(item1).unwrap();
            let o2 = world.collision_object(item2).unwrap();

            println!("Contact between {} and {}", o1.data(), o2.data());
        }
    }

    // Open the window and render.
    let mut camera = ArcBall::new(Point3::new(0.0, 40.0, 150.0), Point3::new(0.0, 40.0, 0.0));
    while window.render_with_camera(&mut camera) {}
}
